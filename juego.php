<?php
use DB\SQL\Mapper;

class Juego
{
	static function puntaje() {
		$f3 = \Base::instance();
		$db = $f3->get('DB');

		$prospecto = $f3->get('PARAMS.prospecto');

		$puntaje = new Mapper($db,'puntaje');
		$puntajes = $puntaje->select("evento, puntos",
			[
				"prospecto = :id",
				// "prospecto = :id AND evento LIKE 'juego-_'",
				":id" => $prospecto
			],
			[
				'order' => "timestamp DESC",
				"limit" => 3
			]
		);

		// $puntos = array_map(function ($item){
		// 	return [
		// 		"evento" => $item->evento,
		// 		"puntos" => $item->puntos
		// 	];
		// }, $puntajes);

		$cuenta = 0;
		foreach ($puntajes as $item) {
			if (comienzaCon($item->evento, "juego-")) {
				$cuenta += $item->puntos;
			}
			else{
				break;
			}
		}


		$puntajes = new Mapper($db,'puntajes');
		$total = $puntajes->findone([
			"prospecto=:id",
			":id" => $prospecto,
		]);

		header('Content-Type: application/json');
		echo json_encode([
			"total" => intval($total->puntos?:0),
			"juego" => $cuenta
		]);
	}




}


function comienzaCon($cadena, $valor)
{
	return (strpos($cadena, $valor) === 0);
}
