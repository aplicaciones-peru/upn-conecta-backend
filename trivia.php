<?php
use DB\SQL\Mapper;

class Trivia
{
	static function success() {
		$f3 = \Base::instance();
		$db = $f3->get('DB');

		$u = $f3->get('PARAMS.u');
		$p = $f3->get('PARAMS.p');
		$f = $f3->get('PARAMS.f');
		header('Content-Type: application/json');

		$trivia = new Mapper($db,'trivia');
		$trivia->usuario = $u;
		$trivia->pregunta = $p;
		$trivia->facultad = $f;
		$trivia->save();

		$puntaje = new Mapper($db,'puntaje');

		$r = $puntaje->load([
			"prospecto = :id AND evento = :evento",
			":id" => $u,
			":evento" => "juego-$p",
		]);

		$rpta = (object)[];

		if( $puntaje->dry() ){
			$puntaje->puntos = $p<=2 ? 25 : 50 ;
			$puntaje->evento = "juego-$p";
			$puntaje->prospecto = $u;
			$puntaje->save();
		}

		if ($trivia){
			echo json_encode(["estado"=> "OK", "mensaje" => "insertado"]);
		}

	}

}
