-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Create table `ubigeo`
--
CREATE TABLE IF NOT EXISTS ubigeo (
  id char(6) NOT NULL,
  departamento varchar(50) NOT NULL,
  provincia varchar(50) NOT NULL,
  distrito varchar(50) NOT NULL,
  capital varchar(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 87,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `prospecto`
--
CREATE TABLE IF NOT EXISTS prospecto (
  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  guid varchar(36) DEFAULT NULL,
  nombres varchar(32) NOT NULL,
  apellido_paterno varchar(32) NOT NULL,
  apellido_materno varchar(32) NOT NULL,
  email varchar(128) NOT NULL,
  celular varchar(9) DEFAULT NULL,
  telefono1 varchar(9) DEFAULT NULL,
  telefono2 varchar(9) DEFAULT NULL,
  telefono3 varchar(9) DEFAULT NULL,
  distrito char(6) DEFAULT NULL,
  creacion timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  modificacion timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  actualizado datetime DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 210975,
AVG_ROW_LENGTH = 161,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `UK_prospecto_guid` on table `prospecto`
--
ALTER TABLE prospecto
ADD UNIQUE INDEX UK_prospecto_guid (guid);

--
-- Create foreign key
--
ALTER TABLE prospecto
ADD CONSTRAINT FK_prospecto_distrito FOREIGN KEY (distrito)
REFERENCES ubigeo (id);

--
-- Create table `puntaje`
--
CREATE TABLE IF NOT EXISTS puntaje (
  prospecto int(11) UNSIGNED NOT NULL,
  evento varchar(64) NOT NULL,
  puntos int(11) UNSIGNED NOT NULL,
  timestamp timestamp NULL DEFAULT CURRENT_TIMESTAMP
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 103,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `IDX_puntaje_evento` on table `puntaje`
--
ALTER TABLE puntaje
ADD FULLTEXT INDEX IDX_puntaje_evento (evento) WITH PARSER NGRAM;

--
-- Create index `UK_puntaje` on table `puntaje`
--
ALTER TABLE puntaje
ADD UNIQUE INDEX UK_puntaje (prospecto, evento);

--
-- Create foreign key
--
ALTER TABLE puntaje
ADD CONSTRAINT FK_puntaje_prospecto FOREIGN KEY (prospecto)
REFERENCES prospecto (id);

--
-- Create view `puntajes`
--
CREATE OR REPLACE
VIEW puntajes
AS
SELECT
  `prospecto`.`id`                     AS `prospecto`,
  COALESCE(SUM(`puntaje`.`puntos`), 0) AS `puntos`
  FROM
    (`prospecto`
      LEFT JOIN `puntaje`
        ON ((`puntaje`.`prospecto` = `prospecto`.`id`)))
GROUP BY `puntaje`.`prospecto`,
         `prospecto`.`id`;

--
-- Create view `Ranking`
--
CREATE OR REPLACE
VIEW Ranking
AS
SELECT
  `p1`.`guid`             AS `guid`,
  `p1`.`nombres`          AS `nombres`,
  `p1`.`apellido_paterno` AS `apellido_paterno`,
  `p1`.`apellido_materno` AS `apellido_materno`,
  `p`.`puntos`            AS `puntos`
  FROM
    (`puntajes` `p`
      LEFT JOIN `prospecto` `p1`
        ON ((`p`.`prospecto` = `p1`.`id`)))
WHERE (`p`.`puntos` > 0)
ORDER BY `p`.`puntos` DESC;

--
-- Create table `evento`
--
CREATE TABLE IF NOT EXISTS evento (
  timestamp timestamp NOT NULL,
  url char(128) DEFAULT NULL,
  categoria char(64) DEFAULT NULL,
  nombre char(128) DEFAULT NULL,
  ip char(15) DEFAULT NULL,
  agente char(255) DEFAULT NULL,
  token char(127) DEFAULT NULL
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 796,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create view `eventos`
--
CREATE OR REPLACE
VIEW eventos
AS
SELECT
  SUBSTRING_INDEX(`e`.`url`, '?', 1) AS `url`,
  `e`.`categoria`                    AS `categoria`,
  `e`.`nombre`                       AS `nombre`,
  COUNT(`e`.`timestamp`)             AS `valor`
  FROM
    `evento` `e`
WHERE LENGTH(`e`.`nombre`)
GROUP BY SUBSTRING_INDEX(`e`.`url`, '?', 1),
         `e`.`categoria`,
         `e`.`nombre`;

--
-- Create table `trivia`
--
CREATE TABLE IF NOT EXISTS trivia (
  id int(11) NOT NULL AUTO_INCREMENT,
  usuario int(11) DEFAULT NULL,
  pregunta int(11) DEFAULT NULL,
  facultad int(11) DEFAULT NULL,
  fecha timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2132,
AVG_ROW_LENGTH = 61,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `sesion`
--
CREATE TABLE IF NOT EXISTS sesion (
  token varchar(127) NOT NULL,
  prospecto int(11) UNSIGNED NOT NULL,
  iniciado timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `prospecto_horario`
--
CREATE TABLE IF NOT EXISTS prospecto_horario (
  prospecto int(11) UNSIGNED NOT NULL,
  horario int(11) UNSIGNED NOT NULL
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `membership`
--
CREATE TABLE IF NOT EXISTS membership (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_name varchar(255) DEFAULT NULL,
  pass_word varchar(32) DEFAULT NULL,
  pass_word_visible varchar(500) DEFAULT NULL,
  first_name varchar(255) DEFAULT NULL,
  last_name varchar(255) DEFAULT NULL,
  fecha_creacion datetime DEFAULT NULL,
  fecha timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  user_hash text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 467,
AVG_ROW_LENGTH = 16384,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `horario`
--
CREATE TABLE IF NOT EXISTS horario (
  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  inicio datetime NOT NULL,
  fin datetime NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `historial`
--
CREATE TABLE IF NOT EXISTS historial (
  timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  evento varchar(63) NOT NULL,
  valor varchar(63) NOT NULL,
  resultado varchar(63) NOT NULL
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 65,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `contacto`
--
CREATE TABLE IF NOT EXISTS contacto (
  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  asesor varchar(128) NOT NULL,
  celular char(9) NOT NULL,
  ciudad varchar(16) NOT NULL,
  ubiigeo char(2) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 11,
AVG_ROW_LENGTH = 1638,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `cita`
--
CREATE TABLE IF NOT EXISTS cita (
  id int(11) NOT NULL AUTO_INCREMENT,
  usuario int(11) DEFAULT NULL,
  nombre_colegio varchar(500) DEFAULT NULL,
  plataforma varchar(400) DEFAULT NULL,
  fecha_hora varchar(400) DEFAULT NULL,
  fecha timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 239,
AVG_ROW_LENGTH = 206,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;