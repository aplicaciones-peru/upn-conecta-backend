<?php
use DB\SQL\Mapper;

class Prospectos
{
	static function buscar() {
		// echo 'API servicios buscar';
		$f3 = \Base::instance();
		$db = $f3->get('DB');

		$valor = $f3->get('PARAMS.valor');
		// $telefono = $f3->get('PARAMS.telefono');
		// $correo = $f3->get('PARAMS.correo');

		$prospectos = new Mapper($db,'prospecto');
		$prospecto = $prospectos->findone([
			"(celular=:valor OR email = :valor) AND guid IS NULL",
			":valor"=>$valor
		]);

		header('Content-Type: application/json');
		if ($prospecto){
			$puntajes = new Mapper($db,'puntajes');
			$datos = $puntajes->findone([
				"prospecto=:id",
				":id" => $prospecto->id,
			]);

			$puntaje = new Mapper($db,'puntaje');
			$trivia = $puntaje->findone([
				"prospecto=:id AND evento LIKE 'trivia-_'",
				":id" => $prospecto->id,
			],[
				'order' => "timestamp DESC",
			]);

			$historial = new Mapper($db,'historial');
			$historial->evento = "buscar";
			$historial->valor = $valor;
			$historial->resultado = empty($prospecto->actualizado) ? "encontrado":"actualizado";
			$historial->save();

			echo json_encode([
				"estado"=> "OK",
				"datos" => [
					"id" => $prospecto->id,
					"guid" => $prospecto->guid,
					"nombres" => $prospecto->nombres,
					"apellidoPaterno" => $prospecto->apellido_paterno,
					"apellidoMaterno" => $prospecto->apellido_materno,
					"email" => $prospecto->email,
					"celular" => $prospecto->celular,
					"telefono1" => $prospecto->telefono1,
					"telefono2" => $prospecto->telefono2,
					"telefono3" => $prospecto->telefono3,
					"puntos" => intval($datos->puntos),
					"actualizado" => !empty($prospecto->actualizado),
					"nivel" => intval(str_replace("trivia-", "", $trivia->evento))
				]
			]);
		}
		else{

			$historial = new Mapper($db,'historial');
			$historial->evento = "buscar";
			$historial->valor = $valor;
			$historial->resultado = "no encontrado";
			$historial->save();

			echo json_encode(["estado"=> "ERROR", "mensaje" => "no encontrado"]);
			// echo "NO";
			// exit();
		}
	}



	static function actualizar() {
		$f3 = \Base::instance();
		$db = $f3->get('DB');
		$json = json_decode($f3->get('BODY'));

		$id = $f3->get('PARAMS.id');

		$prospecto = new Mapper($db,'prospecto');
		$prospecto->load([
			"id=:id",
			":id"=>$id
		]);

		$json->apellido_paterno = $json->apellidoPaterno;
		$json->apellido_materno = $json->apellidoMaterno;
		$json->actualizado = Date("Y-m-d H:i:s");

		$prospecto->copyfrom($json);
		$prospecto->save();


		$historial = new Mapper($db,'historial');
		$historial->evento = "actualizar";
		$historial->valor = $id;
		$historial->resultado = "actualizado";
		$historial->save();

		header('Content-Type: application/json');
		echo json_encode(["estado"=> "OK", "mensaje" => "actualizado"]);
	}


	static function registrar() {
		$f3 = \Base::instance();
		$db = $f3->get('DB');
		$json = json_decode($f3->get('BODY'));

		$id = $f3->get('PARAMS.id');

		$prospecto = new Mapper($db,'prospecto');
		$prospecto->load([
			"id=:id",
			":id"=>$id
		]);

		$json->apellido_paterno = $json->apellidoPaterno;
		$json->apellido_materno = $json->apellidoMaterno;
		$json->actualizado = Date("Y-m-d H:i:s");

		$prospecto->copyfrom($json);
		$prospecto->save();


		$historial = new Mapper($db,'historial');
		$historial->evento = "registro";
		$historial->valor = $json->celular;
		$historial->resultado = "registrado";
		$historial->save();

		header('Content-Type: application/json');
		echo json_encode([
			"estado"=> "OK",
			"mensaje" => "actualizado",
			"datos" => [
				"id" => $prospecto->id,
				"guid" => $prospecto->guid,
				"nombres" => $prospecto->nombres,
				"apellidoPaterno" => $prospecto->apellido_paterno,
				"apellidoMaterno" => $prospecto->apellido_materno,
				"email" => $prospecto->email,
				"celular" => $prospecto->celular,
				"telefono1" => $prospecto->telefono1,
				"telefono2" => $prospecto->telefono2,
				"telefono3" => $prospecto->telefono3,
				"puntos" => 0,
				"actualizado" => !empty($prospecto->actualizado),
				"nivel" => 0
			]
		]);
	}


	static function puntaje() {
		$f3 = \Base::instance();
		$db = $f3->get('DB');
		$json = json_decode($f3->get('BODY'));

		$id = (int)$f3->get('PARAMS.id');

		$puntaje = new Mapper($db,'puntaje');

		$r = $puntaje->load([
			"prospecto = :id AND evento = :evento",
			":id" => $id,
			":evento" => $json->evento,
		]);

		$rpta = (object)[];

		if( $puntaje->dry() ){
			$puntaje->copyfrom($json);
			$puntaje->prospecto = $id;
			$puntaje->save();
			$rpta->estado = "OK";
			$rpta->mensaje = "Puntos registrados con exito";
		}
		else{
			$rpta->estado = "ERROR";
			$rpta->mensaje = "Evento ya registrado para este usuario";
		}


		$puntajes = new Mapper($db,'puntajes');
		$datos = $puntajes->findone([
			"prospecto=:id",
			":id" => $id
		]);
		$rpta->puntos = $datos->puntos;

		header('Content-Type: application/json');
		echo json_encode($rpta);
	}

	static function puntajeTotal() {
		$f3 = \Base::instance();
		$db = $f3->get('DB');

		$id = (int)$f3->get('PARAMS.id');

		$puntaje = new Mapper($db,'puntaje');

		$rpta = (object)[];

		$puntajes = new Mapper($db,'puntajes');
		$datos = $puntajes->findone([
			"prospecto=:id",
			":id" => $id
		]);
		$rpta->puntos = $datos->puntos;

		header('Content-Type: application/json');
		echo json_encode($rpta);

	}

}
