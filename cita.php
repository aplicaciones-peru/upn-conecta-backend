<?php
use DB\SQL\Mapper;

class Cita
{
	static function crear() {
		$f3 = \Base::instance();
		$db = $f3->get('DB');
		$json = json_decode($f3->get('BODY'));
		
		header('Content-Type: application/json');
		
		$trivia = new Mapper($db,'cita');
		$trivia->usuario = $json->usuario;
		$trivia->nombre_colegio = $json->nombre_colegio;
		$trivia->fecha_hora = $json->fecha_hora;
		$trivia->plataforma = $json->plataforma;
		$trivia->save();
		
		if ($trivia){
			echo json_encode(["estado"=> "OK", "mensaje" => "insertado"]);
		}
		
		
	}

}
