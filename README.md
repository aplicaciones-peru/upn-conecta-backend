# UPN Conecta : Servicios
_Servicios de datos para la aplicacion UPN Conecta_
## Comenzando 🚀

### Pre-requisitos
* Servidor Web
* [MySql](https://www.mysql.com/) 5.6+
* [PHP](https://www.php.net/) 7.1+
* [Composer](https://getcomposer.org/)


### Instalación

_Clonar el proyecto en la carpeta de destino_
```
$ git clone <url git> conecta
```

_Instalar las dependencias_
```
$ composer install
```

_Crear la estructura de la base de datos usando el archivo `db/estructura.sql`_

_Configurar lo valores de entorno_
```
# config.ini
[globals]
DB_HOST=conecta-instance-1.cc4ngw3j0dii.us-east-2.rds.amazonaws.com
DB_NAME=api
DB_PUERTO=3306
DB_USUARIO=admin
DB_CLAVE=Conecta2021
```

## Despliegue
_El servidor web debe tener habilitado y configurado el módulo de reescritura de direcciones_

Para Apache:
```
#.htaccess
RewriteEngine On
RewriteBase /upn/servicios/

RewriteRule ^(app|dict|ns|tmp)\/|\.ini$ - [R=404]

RewriteCond %{REQUEST_FILENAME} !-l
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule .* index.php [L,QSA]
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization},L]
```


## Construido con
* [PHP](https://www.php.net/)
* [Composer](https://getcomposer.org/)
* [F3](https://fatfreeframework.com/3.7/home)

---
