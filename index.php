<?php

require 'vendor/autoload.php';
require 'prospectos.php';
require 'trivia.php';
require 'cita.php';
require 'juego.php';
require 'analitica.php';


$f3 = \Base::instance();
$f3->config('config.ini', true);
// $f3->set('DEBUG', 1);
$f3->copy("HEADERS.Origin","CORS.origin");

$f3->set('DB', new DB\SQL(
	"mysql:host={$f3->DB_HOST};port={$f3->DB_PUERTO};dbname={$f3->DB_NAME}",
	$f3->DB_USUARIO,
	$f3->DB_CLAVE
));

$f3->route('GET /',
	function() {
		echo 'API servicios';
	}
);

$f3->route('GET /prospecto/buscar/@valor', "Prospectos::buscar");
$f3->route('PUT /prospecto/@id', "Prospectos::actualizar");
$f3->route('POST /prospecto/@id', "Prospectos::actualizar");
$f3->route('POST /prospecto', "Prospectos::registrar");

$f3->route('POST /cita/crear', "Cita::crear");

$f3->route('POST /puntaje/@id', "Prospectos::puntaje");

$f3->route('GET /trivia/success/@p/@u/@f', "Trivia::success");

$f3->route('GET /juego/@prospecto', "Juego::puntaje");

$f3->route('POST /analitica/registrar', "Analitica::guardar");
$f3->route('GET /analitica/registrar', "Analitica::guardar");

$f3->set("CORS.headers", "X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Cache-Control, Authorization, X-Auth-Token");
$f3->set("CORS.origin", "*");
$f3->set("CORS.credentials", true);

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

if ($f3->hive['VERB']=='OPTIONS'){
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: token');
}

// header('Access-Control-Allow-Origin: *');
// header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Cache-Control, Authorization, X-Auth-Token");
// header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
// header("Allow: GET, POST, PATCH, PUT, DELETE, OPTIONS'");


$f3->run();
