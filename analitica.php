<?php
use DB\SQL\Mapper;

class Analitica
{
	static function guardar() {
		$f3 = \Base::instance();
		$db = $f3->get('DB');
		$json = json_decode($f3->get('BODY'));

		$ip = self::obtenerIP();
		$agente = $_SERVER['HTTP_USER_AGENT'];
		$token = md5($ip) ."|". md5($agente);

		$evento = new Mapper($db,'evento');

		$evento->timestamp = date('Y-m-d H:i:s');
		$evento->agente = $agente;
		$evento->ip = $ip;
		$evento->url = $json->url?:"";
		$evento->categoria = $json->categoria?:"";
		$evento->nombre = $json->nombre?:"";
		$evento->token = $token;
		$evento->save();

		header('Content-Type: application/json');
		echo json_encode(["estado"=> "OK", "token" => $token]);
		// echo json_encode(self::obtenerIP());
		// echo json_encode(self::obtenerIP());
	}

	static function obtenerIP(){
		$ip = '';
		if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])){
			$ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
		}
		else if (isset($_SERVER['HTTP_CLIENT_IP'])){
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else if(isset($_SERVER['HTTP_X_FORWARDED'])){
			$ip = $_SERVER['HTTP_X_FORWARDED'];
		}
		else if(isset($_SERVER['HTTP_FORWARDED_FOR'])){
			$ip = $_SERVER['HTTP_FORWARDED_FOR'];
		}
		else if(isset($_SERVER['HTTP_FORWARDED'])){
			$ip = $_SERVER['HTTP_FORWARDED'];
		}
		else if(isset($_SERVER['REMOTE_ADDR'])){

			$ip = $_SERVER['REMOTE_ADDR'];
		}
		else{
			$ip = 'UNKNOWN';
		}
		return $ip;
	}

}
